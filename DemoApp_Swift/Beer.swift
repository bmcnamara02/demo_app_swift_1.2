//
//  Beer.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import Foundation

struct Beer {
    let name: String;
    let ibu: String;
    let abv: String;
    let labelImageURL: String
}