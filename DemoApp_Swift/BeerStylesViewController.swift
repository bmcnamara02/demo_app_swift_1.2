//
//  BeerStylesViewController.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class BeerStylesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // set via storyboard
    @IBOutlet var beerStylesViewModel: BeerStylesViewModel!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        beerStylesViewModel.fetchBeerStyles {
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beerStylesViewModel.numberOfItemsInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! BeerStylesTableViewCell
        
        configureCell(cell, forRowAtIndexPath: indexPath)
        
        return cell
    }
    
    
    func configureCell(cell: BeerStylesTableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.titleLabel.text = beerStylesViewModel.titleForItemAtIndexPath(indexPath);
        cell.subLabel.text = beerStylesViewModel.shortNameForItemAtIndexPath(indexPath)
        cell.descriptionLabel.text = beerStylesViewModel.descriptionForItemAtIndexPath(indexPath)
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let selectedIndex = self.tableView.indexPathForCell(sender as! UITableViewCell)
        if segue.identifier == "showBeers" {
            if let viewController: BeersViewController = segue.destinationViewController as? BeersViewController {
                let theIndex: Int = beerStylesViewModel.styleIdForItemAtIndexPath(selectedIndex!)
                viewController.beerStyle = theIndex
            }
        }
    }

    
}
