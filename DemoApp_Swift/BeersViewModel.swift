//
//  BeersViewModel.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class BeersViewModel: NSObject {
    
    @IBOutlet var apiClient: APIClient!
    var beers: [Beer]?
    
    func fetchBeers(beerStyleIndex: Int, completion: () -> () ) {
        apiClient.fetchBeers(beerStyleIndex, completion: { beers in
            self.beers = beers
            print(beers)
            completion()
        })
    }

    func numberOfItemsInSection(section: Int) -> Int {
        return beers?.count ?? 0
    }
    
    func nameForItemAtIndexPath(indexpath: NSIndexPath) -> String {
        return beers?[indexpath.row].name ?? ""
    }
    
    func ibuForItemAtIndexPath(indexpath: NSIndexPath) -> String {
        return beers?[indexpath.row].ibu ?? ""
    }
    
    func abvForItemAtIndexPath(indexPath: NSIndexPath) -> String {
        return beers?[indexPath.row].abv ?? ""
    }
    
}
