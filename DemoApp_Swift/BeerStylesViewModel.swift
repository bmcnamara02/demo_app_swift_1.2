//
//  BeerStylesViewModel.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class BeerStylesViewModel: NSObject {
    
    @IBOutlet var apiClient: APIClient!
    var beerStyles: [BeerStyle]?
    
    func fetchBeerStyles(completion: () -> () ) {
        apiClient.fetchBeerStyles { beerStyles in
            self.beerStyles = beerStyles
            completion()
        }
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return beerStyles?.count ?? 0
    }
    
    func titleForItemAtIndexPath(indexpath: NSIndexPath) -> String {
        return beerStyles?[indexpath.row].title ?? ""
    }
    
    func descriptionForItemAtIndexPath(indexpath: NSIndexPath) -> String {
        return beerStyles?[indexpath.row].description ?? ""
    }
    
    func shortNameForItemAtIndexPath(indexPath: NSIndexPath) -> String {
        return beerStyles?[indexPath.row].shortName ?? ""
    }
    
    func styleIdForItemAtIndexPath(indexPath: NSIndexPath) -> Int {
        return beerStyles?[indexPath.row].styleId ?? 0
    }
   
}
