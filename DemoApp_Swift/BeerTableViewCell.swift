//
//  BeerTableViewCell.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 14/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class BeerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var beerIBULabel: UILabel!
    @IBOutlet weak var beerABVLabel: UILabel!
    @IBOutlet weak var beerImageView: UIImageView!
    @IBOutlet weak var beerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
