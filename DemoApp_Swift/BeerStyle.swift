//
//  BeerStyle.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import Foundation

struct BeerStyle {
    let title: String
    let description: String
    let shortName: String
    let styleId: Int
}

