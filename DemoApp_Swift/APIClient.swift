//
//  APIClient.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class APIClient: NSObject {
    
    let API_KEY: String = "48e87be59cbbe6b1403dca290c0f0760"
    
    func fetchBeerStyles(completion: ([BeerStyle]?) -> () ) {
        // fetch data

        
        let urlString: String = "http://api.brewerydb.com/v2/styles?key=\(API_KEY)"
        let url = NSURL(string: urlString)!
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        let task = session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                completion(nil)
                return
            }
            
            // TODO: check HTTP response status code
            
            var error: NSError?
            
            if let json = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as? NSDictionary {
                if let beerStylesJSON = json.valueForKeyPath("data") as? [NSDictionary] {
                    let beerStyles = beerStylesJSON.map { self.parseBeerStyle($0) }
                    completion(beerStyles)
                    return
                }
            }
        })
        
        task.resume()
        
    }
    
    func fetchBeers(beerStyleIndex: Int, completion: ([Beer]?) -> () ) {
        let urlString: String = "http://api.brewerydb.com/v2/beers?styleId=\(beerStyleIndex)&withBreweries=Y&key=\(API_KEY)"
        let url = NSURL(string: urlString)!
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        let task = session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                completion(nil)
                return
            }
            
            // TODO: check HTTP response status code
            
            var error: NSError?
            
            if let json = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: &error) as? NSDictionary {
                if let beerJSON = json.valueForKeyPath("data") as? [NSDictionary] {
                    let beers = beerJSON.map { self.parseBeer($0) }
                    completion(beers)
                    return
                }
            }
        })
        
        task.resume()

    }
    
    func parseBeerStyle(beerStyle: NSDictionary) -> BeerStyle {
        
        let beerStyleName: String = beerStyle.valueForKeyPath("name") as! String
        var beerStyleDescription = (beerStyle.valueForKeyPath("description") as? String)
        let beerStyleType = (beerStyle.valueForKeyPath("shortName") as! String)
        let beerStyleId = (beerStyle.valueForKeyPath("id") as! Int)
        
        // there's an empty description parameter in the json which was
        // causing a crash. solution may be a bit of a hack, but it works...
        if beerStyleDescription == nil {
            beerStyleDescription = ""
        }
        
        return BeerStyle(title:beerStyleName, description:beerStyleDescription!, shortName:beerStyleType, styleId:beerStyleId)
    }
    
    func parseBeer(beer: NSDictionary) -> Beer {
        let beerName: String = beer.valueForKeyPath("name") as! String
        var beerIbu = (beer.valueForKeyPath("ibu") as? String)
        var beerAbv = beer.valueForKeyPath("abv") as? String
        var beerImageURL = beer.valueForKeyPath("labelImageURL") as? String
        
        if beerIbu == nil {
            beerIbu = "n/a"
        }
        
        if beerAbv == nil {
            beerAbv = "n/a"
        }
        
        if beerImageURL == nil {
            beerImageURL = ""
        }
        
        return Beer(name:beerName, ibu:beerIbu!, abv:beerAbv!, labelImageURL: beerImageURL!)
    }
    
}
