//
//  BeersViewController.swift
//  DemoApp_Swift
//
//  Created by Barry McNamara on 13/09/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

import UIKit

class BeersViewController: UIViewController, UITableViewDataSource {
    var beerStyle: Int?
    
    @IBOutlet var beersViewModel: BeersViewModel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        beersViewModel.fetchBeers(beerStyle!, completion: { () -> () in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beersViewModel.numberOfItemsInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("beerCell", forIndexPath: indexPath) as! BeerTableViewCell
        
        configureCell(cell, forRowAtIndexPath: indexPath)
        
        return cell
    }
    
    func configureCell(cell: BeerTableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.beerNameLabel!.text = beersViewModel.nameForItemAtIndexPath(indexPath)
        cell.beerABVLabel!.text = "ABV: \(beersViewModel.abvForItemAtIndexPath(indexPath))"
        cell.beerIBULabel!.text = "IBU: \(beersViewModel.ibuForItemAtIndexPath(indexPath))"
        
        var image: UIImage = UIImage(named: "beer-icon.png")!
        cell.beerImageView.image = image

    }
}
